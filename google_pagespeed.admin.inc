<?php

/**
 * @file
 * Admin page callbacks for the google pagespeed module.
 */

/**
 * Builds and returns the pagespeed settings form.
 *
 * @ingroup forms
 */
function google_pagespeed_admin_settings() {
  $form['google_pagespeed_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google API key'),
    '#default_value' => variable_get('google_pagespeed_api_key', ''),
    '#required' => TRUE,
    '#description' => t('Please make sure you read the terms of service: !termslink', array('!termslink' => "https://developers.google.com/terms/")),
  );

  return system_settings_form($form);
}
